package com.mars.homework.shop;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		
		System.out.println();
		System.out.println("-----Welcome to AD1988 shopping-----");
		System.out.println();
	
		Shop shop = new Shop("AD1988","Oak Creek, Wisconsin",new String[] {"Shoes","Handbags","Clothing","Beauty","Jewelry","Toys"});
		ArrayList<Shop> shopArray = new ArrayList<Shop>();
		shopArray.add(shop);
		
		
		System.out.println("What would you like to shop today?");
		System.out.println();
	
	
		
		try (Scanner sc = new Scanner(System.in)) {
			
			String enteredString = sc.nextLine(); 
			
			Shop.productName = enteredString;
			System.out.println();
			
			String[] allProducts = shop.getProducts();
			int q = 0;
			int count = 0;
			while(q<allProducts.length)
			{
				if(enteredString.toLowerCase().equals(allProducts[q].toLowerCase())) {
					count =1;
			    System.out.println("Yes. You have "+allProducts[q]+" in the list");
				}
			    q++;
			}
			if(count == 0) {
				System.out.println("Thank you for shopping with "+shop.getShopName()+" located at "+shop.getShopAddress()+". You have zero "+enteredString+" in the store");
			}
		}
	}
}
