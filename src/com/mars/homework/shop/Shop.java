package com.mars.homework.shop;

public class Shop {
	
	static String productName;
	private String shopName;
	private String shopAddress;
	private String[] products;
	
	public Shop(String shopName, String shopAddress, String[] products) {
		super();
		this.shopName = shopName;
		this.shopAddress = shopAddress;
		this.products = products;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public String[] getProducts() {
		return products;
	}
	public void setProducts(String[] products) {
		this.products = products;
	}
	
	
}
