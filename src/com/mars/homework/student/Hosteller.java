package com.mars.homework.student;

public class Hosteller extends Student {
	
	private int roomNumber;
	private int phoneNumber;
	public int getRoomNumber() {
		return roomNumber;
	}
	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}
	public int getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Student Name : "+String.valueOf(getStudentName())
				+" Student ID :"+String.valueOf(getStudentID())
				+" Room number :"+this.roomNumber
				+" Phone number:"+this.phoneNumber;
	}
}
