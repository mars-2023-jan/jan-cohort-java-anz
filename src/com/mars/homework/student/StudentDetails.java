package com.mars.homework.student;

import java.util.Scanner;

public class StudentDetails {
	
	public static void main(String[] args) {
		
		Hosteller hostellerObj = new Hosteller();
		hostellerObj.setStudentID(1932);
		hostellerObj.setStudentName("Philip");
		System.out.println();
		System.out.println(hostellerObj);
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("------------------------------");
		
		System.out.println("Enter the new phone number: ");
		
		int updatedPhNum = sc.nextInt();
		hostellerObj.setPhoneNumber(updatedPhNum);
		
		System.out.println();
		System.out.println("Enter the new room number: ");
		
		int updatedRoomNum = sc.nextInt();
		hostellerObj.setRoomNumber(updatedRoomNum);
		
		
		System.out.println("------------------------------");
	
		System.out.println("Updated student details : ");
		System.out.println();
		
		System.out.println("Student Name : "+hostellerObj.getStudentName()
		+" Student ID :"+hostellerObj.getStudentID()
		+" Room number :"+hostellerObj.getRoomNumber()
		+" Phone number:"+hostellerObj.getPhoneNumber());
	
	}

}
