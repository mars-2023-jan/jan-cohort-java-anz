package com.mars.homework.luckynumber;
import java.util.Scanner;

public class LuckyNumber {

	public static void main(String[] args) {
		
		int luckyNumber = 6;
		
		
		int enteredNumber = 0;
		
		for(int  chance = 1; chance <= 3 ; chance++) {
		
			Scanner sc = new Scanner(System.in)	;
		
			System.out.println("Guess any number between 1 and 10:");
		
			enteredNumber = sc.nextInt();
			
			if(chance < 3) {
				
				if(enteredNumber == luckyNumber) {
					
					if(chance == 1) {
					
						System.out.println("You guessed the correct lucky number in "+chance+" chance.");
						
						chance = 3;
					}
					else {
						System.out.println("You guessed the correct lucky number in "+chance+" chances.");
						
						chance = 3;
					}
				}
				else {
					
					System.out.println("You guessed it wrong");
					
				}
				
			}else {
				if(enteredNumber == luckyNumber) {
					
					System.out.println("You guessed the correct lucky number in "+chance+" chances.");
					
				}else {
					
				System.out.println("Game over. You have tried guessing the lucky number for 3 times.");
				
				}
			}
		
		}
		
	}

}
