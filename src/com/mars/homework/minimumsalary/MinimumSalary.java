package com.mars.homework.minimumsalary;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MinimumSalary {

	public static void main(String args[]) {
		
		
		EmployeeArray.companyName = "MARS";
		
		EmployeeArray emp1 = new EmployeeArray("John","Brisbane",10000,101);
		
		EmployeeArray emp2 = new EmployeeArray("Peter","London",9000,102);
		
		EmployeeArray emp3 = new EmployeeArray("Mark","Chicago",8000,103);
		
		EmployeeArray emp4 = new EmployeeArray("Clark","Milwaukee",7000,104);
		
		EmployeeArray emp5 = new EmployeeArray("Dora","Franklin",15000,105);
		
		
		ArrayList<EmployeeArray> empArray = new ArrayList<EmployeeArray>();
		empArray.add(emp1);
		empArray.add(emp2);
		empArray.add(emp3);
		empArray.add(emp4);
		empArray.add(emp5);
		
		System.out.println("------------------------------");
		System.out.println("Employees of MARS are : ");
		System.out.println("------------------------------");
		
		for(EmployeeArray e :empArray) {
			
			System.out.println(e.getEmpDetails());
	       
		}
		
		System.out.println("------------------------------");
		
		EmployeeArray employee =  Collections.min(empArray, Comparator.comparing(emp -> emp.getSalary()));
        System.out.println("The minimum salaried employee is " + employee.getEmpName()+" with a salary of "+employee.getSalary()+".");

	}
}
