package com.mars.homework.minimumsalary;

public class EmployeeArray {
	

	
	static String companyName;
	private String empName;
	private String empAddress;
	private double salary;
	private int empId;
	
	
	public EmployeeArray(String empName, String empAddress, double salary, int empId) {
		super();
		this.empName = empName;
		this.empAddress = empAddress;
		this.salary = salary;
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpAddress() {
		return empAddress;
	}

	public void setEmpAddress(String empAddress) {
		this.empAddress = empAddress;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpDetails() {
		//System.out.prinltn();
		return "Company Name : "+EmployeeArray.companyName+", "+"Employee Name : "+this.empName+", "+" Employee ID : "+this.empId
				+", "+"Employee Salary : "+this.salary;
	}
}
