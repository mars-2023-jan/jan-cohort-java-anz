package com.mars.homework.phonebook;

public class PhoneBook extends Contacts {

	public PhoneBook(String name, String address, String phoneNo) {
		super(name, address, phoneNo);
		
	}
	
	@Override
	public String toString() {
		return "Name: "+String.valueOf(getName())
		+" ,Address: "+String.valueOf(getAddress())
		+" ,Phone number "+String.valueOf(getPhoneNo());
	}
	
	

}
