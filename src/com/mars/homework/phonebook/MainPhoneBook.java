package com.mars.homework.phonebook;



import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

public class MainPhoneBook {
	
	public static void main (String args[]) {
		

		
		Set<PhoneBook> phoneBookList = new HashSet<>();
		
		phoneBookList.add(new PhoneBook("Dav","Oak Creek","999-900-0000"));
		phoneBookList.add(new PhoneBook("John","Franklin","456-010-9000"));
		phoneBookList.add(new PhoneBook("Tim","Milwaukee","783-999-9999"));
		phoneBookList.add(new PhoneBook("Steve","Waukesha","919-888-3214"));
		
		
		System.out.println();
		System.out.println("-------Phone book----------");
		System.out.println();
		
		for(PhoneBook pb:phoneBookList) {
			System.out.println(pb);
		}
	
		System.out.println();
		System.out.println("Enter");
		System.out.println("1. Add a new contact");
		System.out.println("2. Delete a contact");
		System.out.println("3. Update a contact");
		System.out.println("4. Search a contact");
		
		
		Scanner sc = new Scanner(System.in);
		
		int enteredInput = sc.nextInt();
		
		
		if(enteredInput == 1) {
			
			String isAdded = "true";
			
			System.out.println("Enter the contact name : ");
			String name = sc.next();
			
			System.out.println("Enter the contact address : ");
			String address = sc.next();
			System.out.println("Enter the contact number");
			String number = sc.next();
			number = number.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3"); 
			
			phoneBookList.add(new PhoneBook(name.substring(0,1).toUpperCase()+name.substring(1),
											address.substring(0,1).toUpperCase()+address.substring(1),
											number));
			
			System.out.println("-----------------");
			for(PhoneBook pb:phoneBookList) {
			System.out.println(pb);
		}
			
			
			
		}else if(enteredInput == 2) {
			
			System.out.println("Enter the contact to be deleted :");
			String input = sc.next();
			
			 Iterator<PhoneBook> iterator = phoneBookList.iterator();

			    while (iterator.hasNext()) {
			    	PhoneBook phBookIterator = iterator.next();
			    	
			        if (input.equals(phBookIterator.getPhoneNo().replaceAll("[\\s\\-()]", ""))||input.equals(phBookIterator.getName().toLowerCase())) {
			        	
			           iterator.remove();
			        }
			    }

		}else if(enteredInput == 3) {
			
			System.out.println("Enter the name of the contact to be updated :");
			
			String input = sc.next();
			
			String isThere = "false";
			 Iterator<PhoneBook> iterator = phoneBookList.iterator();

			    while (iterator.hasNext()) {
			    	PhoneBook phBookIterator = iterator.next();
			    	
			        if (input.equals(phBookIterator.getName().toLowerCase())) {
			        	
			        	isThere = "true";
			        	
			        	System.out.println("Name: "+phBookIterator.getName()
			        	+" ,Address: "+phBookIterator.getAddress()
			        	+" ,Phone number "+phBookIterator.getPhoneNo());
			        	System.out.println();
			    		System.out.println("Enter");
			    		System.out.println("1. Update the contact name");
			    		System.out.println("2. Update the contact number");
			    		System.out.println("3. Update the contact address");
			    		
			    		int userInput = sc.nextInt();
			    		
			    		if(userInput == 1) {
			    			
			    			System.out.println("Enter the  name : ");
			    			String name = sc.next();
			    			name = name.substring(0,1).toUpperCase()+name.substring(1);
			    			phBookIterator.setName(name);
			    			System.out.println("Name: "+phBookIterator.getName()
				        	+" ,Address: "+phBookIterator.getAddress()
				        	+" ,Phone number "+phBookIterator.getPhoneNo());
				        	System.out.println();
			    			
			    		}else if(userInput == 2) {
			    			
			    			System.out.println("Enter the  number : ");
			    			String inputNum = sc.next();
			    			inputNum = inputNum.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3"); 
			    			phBookIterator.setPhoneNo(inputNum);
			    			System.out.println("Name: "+phBookIterator.getName()
				        	+" ,Address: "+phBookIterator.getAddress()
				        	+" ,Phone number "+phBookIterator.getPhoneNo());
				        	System.out.println();
			    		
			    		}else if(userInput == 3) {
			    			
			    			System.out.println("Enter the  address : ");
			    			String inputAdd = sc.next();
			    			inputAdd = inputAdd.substring(0,1).toUpperCase()+inputAdd.substring(1);
			    			phBookIterator.setAddress(inputAdd);
			    			System.out.println("Name: "+phBookIterator.getName()
				        	+" ,Address: "+phBookIterator.getAddress()
				        	+" ,Phone number "+phBookIterator.getPhoneNo());
				        	System.out.println();
			    		
			    		}else {
			    			isThere = "false";
			    		}
			        }
			    } if(isThere.equals("false")) {
			    	System.out.println("Searched contact is not to be found");
			    }
			
		}else if(enteredInput == 4) {
			
			System.out.println("Enter the contact to be searched :");
			String input = sc.next();
			String isFound = "false";
			 Iterator<PhoneBook> iterator = phoneBookList.iterator();

			    while (iterator.hasNext()) {
			    	
			    	PhoneBook phBookIterator = iterator.next();
			    	
			        if (input.equals(phBookIterator.getPhoneNo().replaceAll("[\\s\\-()]", ""))||input.equals(phBookIterator.getName().toLowerCase())) {
			        	
			        	isFound = "true";
			        	System.out.println("Name: "+phBookIterator.getName()
			        	+" ,Address: "+phBookIterator.getAddress()
			        	+" ,Phone number "+phBookIterator.getPhoneNo());
			        }

			    }
			    if(isFound.equals("false")) {
			    	System.out.println("Searched contact is not to be found");
			    }
			
		}else {
			System.out.println("Wrong entry");
		}

//		for(PhoneBook pb:phoneBookList) {
//			System.out.println(pb);
//		}
			
		
	}


}
