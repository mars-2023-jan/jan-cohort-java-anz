package com.mars.training.banking;
import java.util.InputMismatchException;
import java.util.Scanner;

public class BankDemo{
	
	static SavingsAccount savingsAccountObj = new SavingsAccount();
	static CheckingAccount checkingAccountObj = new CheckingAccount();
	static Scanner sc = new Scanner(System.in);
	static boolean negativeBalance = true;
	static int choiceAccount = 0;
	static int choice = 0;
	static double depAmount = 0;
	static double withdrawAmount= 0;
	static String yesORno;
	static double amtTermDepoist = 0;
	static int choiceTerm = 0;;
	
	
	
	public static void main(String[] args) {
		
		System.out.println("Select your account type:");

		System.out.println("Enter");
				
		System.out.println("1 for Savings");
				
		System.out.println("2 for Checking");
				

		try {
			choiceAccount = sc.nextInt();
			
		}
		catch(InputMismatchException e) {
			
			System.out.println("Wrong entry. Start Over");
			
		}
		
		savingsOrChecking();
				
		
				
	}

	
	public static void savingsOrChecking() {
		
		if (choiceAccount == 1) {
		
		savingsAccountObj.setAccountNumber(11110000);
		savingsAccountObj.setAccHoldername("Mark");
		savingsAccountObj.setAccBalance(2500);
		savingsAccountObj.setMinBalance(200);
		savingsAccountObj.setRoi(0.0425);
		System.out.println(savingsAccountObj);
		depositORwithdraw();
		
		}else if (choiceAccount == 2) {
		
		checkingAccountObj.setAccountNumber(13900000);
		checkingAccountObj.setAccHoldername("Job");
		checkingAccountObj.setAccBalance(3490);
		checkingAccountObj.setMinimumBalance(0);
		checkingAccountObj.setInterestRate(0.0299);
		System.out.println(checkingAccountObj);
		depositORwithdraw();
		
		}

		
	}
	
	
	public static void depositORwithdraw(){
		
		
		double currentSavingsBalance = savingsAccountObj.getAccBalance();
		
		double currentCheckingBalance = checkingAccountObj.getAccBalance();
		
		System.out.println();
		
			if(negativeBalance) {
			
				System.out.println("Enter");
				
				System.out.println("1 for Deposit");
				
				System.out.println("2 for Withdraw");
				
				System.out.println("3 for Term Deposit");
				
				try {
				
					choice = sc.nextInt();
				
				}catch(InputMismatchException e) {
					
					System.out.println("Wrong entry. Start Over");
					
				}
			}
		
		
			if(choice == 1) {
				
				System.out.println("How much do you want to deposit?");
				
				try {
					
						depAmount = sc.nextDouble();
					
				}catch(InputMismatchException e) {
						
						choiceAccount = 0;
						
						System.out.println("Wrong entry. Start Over");		
						
				}
				
				
				if(choiceAccount == 1) {
						
						savingsAccountObj.setDepositAmount(depAmount);
						
						savingsAccountObj.setAccBalance(depAmount+currentSavingsBalance);
						
						System.out.println("You have successfully deposited "+depAmount);
						
						System.out.println();
						
						System.out.println(savingsAccountObj);
						
						
				}else if(choiceAccount == 2) {
						
						checkingAccountObj.setDepositAmount(depAmount);
						
						checkingAccountObj.setAccBalance(depAmount+currentCheckingBalance);
						
						System.out.println("You have successfully deposited "+depAmount);
						
						System.out.println();
						
						System.out.println(checkingAccountObj);
						
				}else {
						System.out.println("Incorrect entry");
						
				}
					
				}else if(choice == 2){
					
					System.out.println("How much do you want to withdraw?");	
					
					withdrawValidation();
					
				}else if(choice == 3){
					
					System.out.println("How much do you want to invest in term deposit?");	
					
					System.out.println("Enter");	
					
					System.out.println("1. Short Term(1-12 months)");
					
					System.out.println("2. Long Term(1-60 months)");
					
					
					try {
							choiceTerm = sc.nextInt();
						}
						catch(InputMismatchException e) {
							
							System.out.println("Wrong entry. Start Over");
							
						}
				
					
					System.out.println();
					
					System.out.println("Enter the amount");	
					
						try {
						
						amtTermDepoist = sc.nextDouble();
						}
						catch(InputMismatchException e) {
							
							System.out.println("Wrong entry. Start Over");
							
						}
				
					
					if(choiceTerm == 1)
					{
						if(choiceAccount == 1) {
							
							System.out.println("After 12 months, you will receive "+amtTermDepoist*savingsAccountObj.getRoi()*12);
							
							double amount = savingsAccountObj.getAccBalance()-(amtTermDepoist*savingsAccountObj.getRoi()*12);
							
							System.out.println("Current Balance : "+amount);
							
							
						}else if(choiceAccount == 2) {
							
							System.out.println(amtTermDepoist*checkingAccountObj.getInterestRate()*12);
							
							double amount = checkingAccountObj.getAccBalance()-(amtTermDepoist*checkingAccountObj.getInterestRate()*12);
							
							System.out.println("Current Balance : "+amount);
							
						}else {
							System.out.println("Caused some error");	
						}
					}else if(choiceTerm == 2){
						
						if(choiceAccount == 1) {
							
							System.out.println(amtTermDepoist*savingsAccountObj.getRoi()*60);
							
							double amount = checkingAccountObj.getAccBalance()-(amtTermDepoist*savingsAccountObj.getRoi()*60);
							
							System.out.println("Current Balance : "+amount);
							
							
						}else if(choiceAccount == 2) {
							
							System.out.println(amtTermDepoist*checkingAccountObj.getInterestRate()*60);
							
							double amount = checkingAccountObj.getAccBalance()-(amtTermDepoist*checkingAccountObj.getInterestRate()*60);
							
							System.out.println("Current Balance : "+amount);
							
							
						}else {
							System.out.println("Caused some error");	
						}
						
					}else {
						
						System.out.println("Incorrect entry");	
						
					}
			
				}
		}
		
		
	
	public static void withdrawValidation() {

	
		try {
			
			withdrawAmount = sc.nextDouble();
			
		}catch(InputMismatchException e) {
			
				choiceAccount = 0;
				System.out.println("Wrong entry. Start Over");
				
			}
		
		
		double presentSavingBal = savingsAccountObj.getAccBalance();
		
		double presentCheckingBal = checkingAccountObj.getAccBalance();
		
		if(choiceAccount ==1) {
			
			if(presentSavingBal<withdrawAmount) {
				
				System.out.println("****Insufficient Balance****");
				
				System.out.println("Do you want to enter another amount. Enter YES or No.");
				
				
				try {
					
					yesORno = sc.next();
				}catch(InputMismatchException e) {
						
						System.out.println("Wrong entry. Start Over");
						
				}
				
				
				if(yesORno.toLowerCase().equals("yes")) {
					
					negativeBalance = false;
					
					choice = 2;
					
					depositORwithdraw();
					
				}else if(yesORno.toLowerCase().equals("no")) {
					
					System.out.println();
					
					System.out.println(savingsAccountObj);
				}else {
					System.out.println();
					
					System.out.println("Incorrect entry");
					
					System.out.println(savingsAccountObj);
				}
				
			}
			
			else {
				System.out.println("You have successfully withdrawn "+withdrawAmount);
				
				savingsAccountObj.setWithdrawAmount(withdrawAmount);
				
				savingsAccountObj.setAccBalance(presentSavingBal-withdrawAmount);
				
				System.out.println();
				
				System.out.println(savingsAccountObj);
				
				
			}
		}else if(choiceAccount == 2) {
				
				if(presentCheckingBal<withdrawAmount) {
					
					System.out.println("****Insufficient Balance****");
					
					System.out.println("Do you want to enter another amount. Enter YES or No");
					
					try {
						
						yesORno = sc.next();
						}
						catch(InputMismatchException e) {
							
							System.out.println("Wrong entry. Start Over");
							
						}
					
					if(yesORno.toLowerCase().equals("yes")) {
						
						negativeBalance = false;
						choice = 2;
						depositORwithdraw();
						
					}else if(yesORno.toLowerCase().equals("no")) {
						System.out.println();
						System.out.println(checkingAccountObj);
					}else {
						System.out.println();
						System.out.println("Incorrect entry");
						System.out.println(checkingAccountObj);
					}
					
				}
				
				else {
					System.out.println("You have successfully withdrawn "+withdrawAmount);
					checkingAccountObj.setWithdrawAmount(withdrawAmount);
					checkingAccountObj.setAccBalance(presentCheckingBal-withdrawAmount);
					System.out.println();
					System.out.println(checkingAccountObj);
					
					
				}
			}
		
	}
	

}
