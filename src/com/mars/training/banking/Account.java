package com.mars.training.banking;

public class Account {
	
	private int accountNumber;
	
	private String accHoldername;
	
	private double accBalance;
	
	private double depositAmount;
	
	private double withdrawAmount;
	
	private double termDeposit;

	public int getAccountNumber() {
		return accountNumber;
	}
	
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public String getAccHoldername() {
		return accHoldername;
	}
	
	public void setAccHoldername(String accHoldername) {
		this.accHoldername = accHoldername;
	}
	
	public double getAccBalance() {
		return accBalance;
	}
	
	public void setAccBalance(double accBalance) {
		this.accBalance = accBalance;
	}

	public double getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(double depositAmount) {
		this.depositAmount = depositAmount;
	}

	public double getWithdrawAmount() {
		return withdrawAmount;
	}

	public void setWithdrawAmount(double withdrawAmount) {
		this.withdrawAmount = withdrawAmount;
	}

	public double getTermDeposit() {
		return termDeposit;
	}

	public void setTermDeposit(double termDeposit) {
		this.termDeposit = termDeposit;
	}
	
	public double calculate(int roi) throws Exception{
		return 12.5 * roi;
	}
	

}
