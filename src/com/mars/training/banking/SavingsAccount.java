package com.mars.training.banking;

public class SavingsAccount extends Account {
	
	private double roi;
	private float minBalance;
	public double getRoi() {
		return roi;
	}
	public void setRoi(double roi) {
		this.roi = roi;
	}
	public float getMinBalance() {
		return minBalance;
	}
	public void setMinBalance(float minBalance) {
		this.minBalance = minBalance;
	}
	@Override
	public String toString() {
		return "Account No: "+String.valueOf(getAccountNumber())
		+" - Account holder name: "+getAccHoldername()
		+" - Account balance: "+String.valueOf(getAccBalance())
		+" - Minimum balance: "+String.valueOf(getMinBalance())
		+" - Rate of interest "+String.valueOf(getRoi());
	}
	public double calculate(int roi, int n) throws Exception{
	return (roi * 15)/n;	
	}
}
