package com.mars.training.banking;

public class CheckingAccount extends Account {
	
	private double interestRate;
	private float minimumBalance;
	public double getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}
	public float getMinimumBalance() {
		return minimumBalance;
	}
	public void setMinimumBalance(float minimumBalance) {
		this.minimumBalance = minimumBalance;
	}
	@Override
	public String toString() {
		return "Account No: "+String.valueOf(getAccountNumber())
		+" - Account holder name: "+getAccHoldername()
		+" - Account balance: "+String.valueOf(getAccBalance())
		+" - Minimum balance: "+String.valueOf(getMinimumBalance())
		+" - Rate of interest "+String.valueOf(getInterestRate());
	}

}
