package com.mars.training.exception;

public class MyException extends Exception{
	
	public MyException(String msg) {
		super(msg);
	}

}
