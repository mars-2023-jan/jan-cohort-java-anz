package com.mars.training;

import java.util.Scanner;

public class DuplicateWords {

	 public static void main(String[] args) {    
		 
		 
		 	System.out.println("Enter your input");  
		 
			Scanner sc = new Scanner(System.in);
			String[]  string = sc.nextLine().toLowerCase().split(" ");
			 
	        int count;    
	            
	        
	            
	        System.out.println("Repeated words are: ");  
	        
	        for(int i = 0; i < string.length; i++) {    
	        	
	            count = 1;    
	            
	            for(int j = i+1; j < string.length; j++) {    
	            	
	                if(string[i].equals(string[j])) {    
	                	
	                    count = count+1;    
	                    
	                    string[j] = "0";    
	                }    
	            }    
	                
	              
	            if(count > 1 && string[i] != "0") {
	            	
	                System.out.println(string[i]+"-"+count); 
	                count = 1;
	            }
	        }    
	    }    
}
