package com.mars.training.java8;

import java.util.function.BiPredicate;

public class LambdaDemo {
	
	public static void main(String args[]) {
		
//		MyFunctionalInterface obj = (a,b) -> a+b;
//		System.out.println(obj.sum(12,12));
		
//		Predicate<Integer> p = (a)->a>7;
//		System.out.println(p.test(9));
//		
//		BiFunction<Integer,Integer,Integer> obj = (a,b) -> a+b;
//		System.out.println(obj.apply(12, 12));
//		
		
		BiPredicate<Integer,Integer> obj1 = (a,b) -> a>b;
		
		System.out.println("A is greter than B : "+obj1.test(12, 11));
		
	}
	

}
