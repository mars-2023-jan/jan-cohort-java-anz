package com.mars.training.java8;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;




public class StreamDemo {

	public static void main(String[] args) {
		

//		List<Integer> numbers = Arrays.asList(2,5,1,6,9,4);
		
		//Stream<Integer> numberStream = numbers.stream();
		
//		numbers.stream()
//		.filter(n->n%2==0)
//		.forEach(x->System.out.println(x));
		
		
		//multiple intermediate operations 
//		numbers.stream()
//		.filter(n->n%2==0)
//		.map(a->a*a)
//		.forEach(x->System.out.println(x));
		
		//sorting the result
//		numbers
//		.stream()
//		.filter(n->n%2==0)
//		.map(a->a*a)
//		.sorted()
//		.forEach(x->System.out.println(x));
		
		//collecting the result
//			List<Integer>	sortedNumbers = 
//					numbers
//				.stream()
//				.filter(n->n%2==0)
//				.map(a->a*a)
//				.sorted()
//				.collect(Collectors.toList());
		//
//			numbers
//			.stream()
//			.filter(n->n%2==0)
//			.map(a->a*a)
//			.forEach(x->System.out.println(x));
			
			List<String> stringList = Arrays.asList("Small","big","Sun");

			stringList
			.stream()
			.filter(a-> a.startsWith("S"))
			.forEach(x->System.out.println(x.toUpperCase()));
			
			stringList
			.stream()
			.filter(a-> a.startsWith("S"))
			.map(a->a.toUpperCase())
			.forEach(System.out::println);
			
			//System.out.println(result);
			
			
		
	}
//	public static void show(String[] msg) {
//		
//		System.out.println(msg);
//		
//	}
//    public static void show() {
//    	
//    }
	//Intermediate operation : transform the stream
	//Terminal operations : end the stream
    
    //Take a list of string values and filter strings starts with s 
	//convert all such string to uppercase and then dispplay them 
}
