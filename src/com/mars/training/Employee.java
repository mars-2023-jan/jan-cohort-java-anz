package com.mars.training;

public class Employee {
	
	
	private String empName;
	private String empAddress;
	private double salary;
	
	private int empId;
	
	public double getSalary() {
		
		return salary;
	}

	public void setSalary(double salary) {
		
		
		this.salary = salary;
		
	}
	
	public String getEmpName() {
		
		return empName;
	}

	public void setEmpName(String empName) {

	
		this.empName = empName;
		
		
	}

	public String getEmpAddress() {
		return empAddress;
	}

	public void setEmpAddress(String empAddress) {
		
		this.empAddress = empAddress;
		
	}



	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		
		this.empId = empId;
		
		
	}

	static String companyName;
	
	public String getEmpDetails() {
		//System.out.prinltn();
		return "Company Name : "+Employee.companyName+", "+"Employee Name : "+this.empName+", "+" Employee ID : "+this.empId
				+", "+"Employee Salary : "+this.salary;
	}
	

}
