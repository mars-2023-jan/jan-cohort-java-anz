package com.mars.training;
import java.util.Scanner;

public class DataDemo {

	public static void main(String[] args) {
		
		byte b = 25;
		
		int i = b; //impilicit typecast
		
		short s = (short)i; // explicit type cast
		
		float f= 30.4f;
		
		DataDemo demo1 = new DataDemo();
		
		int sum = demo1.add(23,25);
		
		System.out.println(sum);
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter first number : ");
		
		int firstNumber = sc.nextInt();
		
		System.out.println("Enter first number : ");
		
		int secondNumber = sc.nextInt();
		
		int total = demo1.add(firstNumber, secondNumber);
		
		System.out.println("Sum is : "+total);
		
		System.out.println("Enter your first name");
		
		String name = sc.next(); // only take strings before the space
		
		System.out.println("Your name is " + name);
		
		
		System.out.println("Your full name");
		
		String fullName = sc.next();
		fullName += sc.nextLine();
		
		//String fullName = sc.nextLine();
		
		System.out.println("Your name is " + fullName);
		
		

	}
	
//	private static int add(int a, int b) {     //Either make this function static
//												//and call this function int sum = add(23,25);
//		return a+b;
//		
//	}
	private int add(int a, int b) {
		
		return a+b;
		
	}

}
