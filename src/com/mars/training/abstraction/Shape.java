package com.mars.training.abstraction;

public abstract class Shape {

	public abstract double area();
}
