package com.mars.training.abstraction;
import java.util.Scanner;

public class TestCarFactory {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the type of the car you want to build:");
		
		String carType = sc.next();
		
		CarFactory.buildCar(CarType.valueOf(carType));
	}

}
