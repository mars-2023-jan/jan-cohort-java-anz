package com.mars.training.abstraction;

public class LuxuryCar extends Cars{

	public LuxuryCar() {
		super(CarType.LUXURY);
		construct();
		
	}
	@Override
	protected void construct() {
		System.out.println("Building sedan car");
	}
}
