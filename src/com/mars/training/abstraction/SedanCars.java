package com.mars.training.abstraction;

public class SedanCars extends Cars {

	public SedanCars() {
		super(CarType.SEDAN);
		construct();
	}
	
	@Override
	protected void construct() {
		System.out.println("Building sedan car");
	}
	

}
