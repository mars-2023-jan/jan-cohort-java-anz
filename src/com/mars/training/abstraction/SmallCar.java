package com.mars.training.abstraction;

public class SmallCar extends Cars {

	public SmallCar() {
		super(CarType.SMALL);
		construct();
	}
	
	@Override
	protected void construct() {
		System.out.println("Building small car");
	}

}
