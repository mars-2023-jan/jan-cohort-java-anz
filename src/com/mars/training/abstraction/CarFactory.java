package com.mars.training.abstraction;

public class CarFactory {
	
	public static Cars buildCar(CarType model) {
		Cars car = null;
		switch (model) {
		
		case SMALL:
			car = new SmallCar();
			break;
		case SEDAN:
			car = new SedanCars();
			break;
		case LUXURY:
			car = new LuxuryCar();
			break;
		default:
			System.out.println("Invlaid CarType");
			
		}
		
		return car;
		
		
	}

}
