package com.mars.training.collections;

import java.util.Objects;

public class StudentCollection implements Comparable<StudentCollection> {
	
	private int studId;
	private String studName;
	private int studAge;
	private String grade;
	
	public StudentCollection(int studId, String studName, int studAge, String grade) {
		super();
		this.studId = studId;
		this.studName = studName;
		this.studAge = studAge;
		this.grade = grade;
	}
	public int getStudId() {
		return studId;
	}
	public void setStudId(int studId) {
		this.studId = studId;
	}
	public String getStudName() {
		return studName;
	}
	public void setStudName(String studName) {
		this.studName = studName;
	}
	public int getStudAge() {
		return studAge;
	}
	public void setStudAge(int studAge) {
		this.studAge = studAge;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	@Override
	public int hashCode() {
		return  studId;
	}
	@Override
	public boolean equals(Object obj) {

		StudentCollection other = (StudentCollection) obj;
		
		return studAge == other.studAge && studId == other.studId && Objects.equals(studName, other.studName);
	}
	@Override
	public int compareTo(StudentCollection o) {
		
//		return this.studName.compareTo(o.getStudName());
		
		if (this.studId>o.getStudId())
			return 1;
		else if(this.studId == o.getStudId())
			return 0;
		else
			return -1;
	}
	@Override
	public String toString() {
		return "Student name: "+this.studName+
				" Student Id: "+this.studId+
				" Student grade: "+this.grade;
	}


}
