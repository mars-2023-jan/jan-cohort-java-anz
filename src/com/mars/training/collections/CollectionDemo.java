package com.mars.training.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;


public class CollectionDemo {

	public static void main(String[] args) {
		
//		
//		Set<StudentCollection> students = new HashSet<>();
//		
//		students.add(new StudentCollection(10,"John",20,"A"));
//		students.add(new StudentCollection(12,"Peter",22,"B"));
//		students.add(new StudentCollection(13,"Sara",20,"C"));
//		students.add(new StudentCollection(11,"David",22,"A"));
//		
//		for(StudentCollection stud : students) {
//			System.out.println(stud.getStudName()+":"+stud.getStudId());
//		}
//		
//		
//		System.out.println("-------Array list-------");
//		
		List<StudentCollection> studList = new ArrayList<>();
//		
//		List<StudentCollection> gradeAStudList = new ArrayList<>();
		
		
		studList.add(new StudentCollection(10,"John",20,"A"));
		studList.add(new StudentCollection(12,"Peter",22,"B"));
		studList.add(new StudentCollection(13,"Sara",20,"C"));
		studList.add(new StudentCollection(11,"David",22,"A"));
	
//		System.out.println(studList.get(3));
//		studList.remove(3);
		
//		for(StudentCollection stud : studList) {
//			
//			System.out.println(stud.getStudName()+":"+stud.getStudId());
//				if(stud.getGrade().equals("A")) {
//					gradeAStudList.add(stud);	
//				}
//			
//		}
//		System.out.println("--------------");
//		System.out.println("Students with grade A are:");
//		
//		for(StudentCollection stud : gradeAStudList) {
//			
//			System.out.println(stud);
//		}
		
		
//		List num = Arrays.asList(2,5,7,9);
//		
//		
//		System.out.println("--------------");
//		Set<StudentCollection> studentsTree = new TreeSet<>();
		
//		studentsTree.add(new StudentCollection(10,"John",20));
//		studentsTree.add(new StudentCollection(11,"David",22));

		
//
//		for(StudentCollection stud : studentsTree) {
//			System.out.println(stud.getStudName());
//		}
//
//		
//		
//		
//		System.out.println("--------------");
//		
//		
//		Set<Integer> names = new TreeSet<>();
//		
//		
//		names.add(10); // each primitive are having corresponding java class
//		
//		names.add(20);//wrapper class
//		
//		names.add(15);//primitive to wrapper class - Boxing
//		
//		names.add(5);// wrapper to primitive - unboxing
		
//		Set<String> names = new HashSet<>();
//		
//		
//		names.add("John");
//		
//		names.add("Sara");
//		
//		names.add("Tom");
//		
//		names.add("John");// does not allow duplicates 
		
//		Iterator<String> it = names.iterator();
//		while(it.hasNext())
//		{
//			System.out.println(it);
//		}
		
//		for(String name : names) {
//			System.out.println(name);
//		}
//
//	Set<String> namesTree = new TreeSet<>();
//		
//	namesTree.add("John");
//		
//	namesTree.add("Sara");
//		
//	namesTree.add("Tom");
//		
//		names.add("John");// does not allow duplicates 
//		
//		for(String name : namesTree) {
//			System.out.println(name);
//		}
		
		
		
		//------------Map-----------
		
		Map<Integer, String> myMap = new HashMap<>();
		myMap.put(101, "Winnie");
		myMap.put(102, "Mark");
		myMap.put(103, "Kate");
		
		//System.out.println(myMap.get(101));
		
		for(Map.Entry<Integer, String> m : myMap.entrySet()) {
			System.out.println(m.getKey()+":"+m.getValue());
		}
		System.out.println("--------------");
		
		
		Map<Integer,StudentCollection> myStudentMap = new HashMap<>();
		
				for(StudentCollection stud : studList) {
					
					myStudentMap.put(stud.getStudId(),stud);
					
					for(Entry<Integer, StudentCollection> m : myStudentMap.entrySet()) {
			
					System.out.println(m.getKey()+":"+m.getValue());
			}
			
		}
		
		
		
	}
	
	

}
