package com.mars.training.collections;

import java.util.Comparator;

public class NameComparator implements Comparator<StudentCollection> {

	@Override
	public int compare(StudentCollection o1, StudentCollection o2) {
		
		return o1.getStudName().compareTo(o2.getStudName());
	}

}
