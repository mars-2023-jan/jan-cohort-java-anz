package com.mars.training.jdbc;

public class Product {

	

	public Product() {
		
	}

	private String proId;
	private String prodName;
	private String prodDesc;
	private double price;
	
	
	
	public Product(String proId, String prodName, String prodDesc, double price) {
		super();
		this.proId = proId;
		this.prodName = prodName;
		this.prodDesc = prodDesc;
		this.price = price;
	}

	public String getProId() {
		return proId;
	}

	public void setProId(String proId) {
		this.proId = proId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getProdDesc() {
		return prodDesc;
	}

	public void setProdDesc(String prodDesc) {
		this.prodDesc = prodDesc;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	
}
