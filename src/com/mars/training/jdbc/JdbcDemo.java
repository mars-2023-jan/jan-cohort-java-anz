package com.mars.training.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class JdbcDemo {

	public static void main(String[] args) throws Exception {
		
		Class.forName("com.mysql.cj.jdbc.Driver");
		
		String url = "jdbc:mysql://localhost:3306/mars_jan?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		
		String user = "root";
		
		String password= "kp15/300A@123$";
		
		Connection con = DriverManager.getConnection(url,user,password);
		
		//--------Check if the connection is issued------
//		if(con!=null) {
//			System.out.println("Connected successfully");
//		}else {
//			System.out.println("Connection refused");
//		}
		
		//-----Call simple queries-----
		
//		Statement stmt = con.createStatement();
//		ResultSet rs = stmt.executeQuery("select * from product");
//		
//		List<Product> prodList = new ArrayList<>();
//		
//		while(rs.next()) {
//			
//			Product product = new Product();
//			product.setProdName(rs.getString(2));
//			product.setProId(rs.getString(1));
//			product.setProdDesc(rs.getString(3));
//			product.setPrice(rs.getDouble(4));
//			
//			prodList.add(product);		
//			System.out.println(rs.getString(2)+":"+rs.getDouble(4)+":"+rs.getString("prod_desc"));
//		}	
//		for(Product prod:prodList) {
//			System.out.println(prod.getProdName()+":"+prod.getProId());
//		}
		
		//------Call dynamic queries-----
		
//		PreparedStatement pStmt = con.prepareStatement("select * from product where prod_id = ?");
//		
//		pStmt.setString(1, "HH101");
//			
//		ResultSet rs = pStmt.executeQuery();
		
//		while(rs.next()) {
//		
//		Product product = new Product();
//		product.setProdName(rs.getString(2));
//		product.setProId(rs.getString(1));
//		product.setProdDesc(rs.getString(3));
//		product.setPrice(rs.getDouble(4));
//		
//		prodList.add(product);		
//	}	
//	for(Product prod:prodList) {
//		System.out.println(prod.getProdName()+":"+prod.getProId());
//	}
		
		//---------Call procedures-------
		
//		CallableStatement cStmt = con.prepareCall("{ call getPriceById(?,?)} ");
//		
//		cStmt.setString(1, "GRINDER");
//		
//		cStmt.registerOutParameter(2, java.sql.Types.DECIMAL);
//		
//		cStmt.executeUpdate();
//		
//		double price = cStmt.getDouble(2);
//		
//		System.out.println(""+price);
//		System.out.println();

		
		//---- Call functions----------	
		
		CallableStatement cStmtFunc = con.prepareCall("{ ? = call getPrice(?)} ");
		
		cStmtFunc.registerOutParameter(1, java.sql.Types.DECIMAL);
		
		cStmtFunc.setString(2, "EL102");
		
		cStmtFunc.executeUpdate();
		
		con.close();
	}

}
