package com.mars.training;

import com.mars.training.exception.MyException;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class ExceptionHandlingDemo {

	public static void main(String[] args) {
//		int x= 0;
		int x= 2;
		try {
		FileReader file = new FileReader("test.txt");
		}
		catch(FileNotFoundException e) {
		}
		
		
		try {
		int y= 6/x;
		
		if(y>2) {
			throw new MyException("My exception caugt");
		}
		}
		catch(ArithmeticException | MyException | NumberFormatException e) {
			
			if(e instanceof MyException) {
				System.out.println("My exception occured");
				
			}else {
			System.out.println("Arithemtic exception occurred");
			}
		}	
		finally {
		System.out.println("Final block");}
	}

}
