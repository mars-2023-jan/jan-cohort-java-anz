package com.mars.training.inheritance;

public class Car extends Vehicle
{
	
	int capacity;
	String brand;
	public Car() {
		System.out.println("Car class called");
	}
	
	public String getVehicleDetails() {
		return super.getVehicleDetails()+" Brand: "+this.brand+" Capacity: "+this.capacity;
	}
}
