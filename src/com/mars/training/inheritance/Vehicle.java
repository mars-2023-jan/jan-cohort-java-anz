package com.mars.training.inheritance;

public class Vehicle {
	
	String vehicleType;
	String color;
	String make;
	boolean isRegistered;
	
	
	public Vehicle() {
		System.out.println("Vehicle class called");
	}
	
	public String getVehicleDetails() {
		return "[Vehicle: "+this.vehicleType
				+" Color: "+this.color
				+" Make: "+this.make
				+" Regsitered:]";
	}
	
}
