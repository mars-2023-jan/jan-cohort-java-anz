package com.mars.training.inheritance;

public class InheritanceDemo {

	public static void main(String[] args) {
		Sedan sedan = new Sedan();
		
		sedan.vehicleType = "Sedan";
		sedan.make ="Ford";
		sedan.brand = "Forrunner";
		sedan.color = "Red";
		sedan.capacity = 4;
		sedan.isRegistered = true;
		
		System.out.println(sedan.getVehicleDetails());
	}
}
