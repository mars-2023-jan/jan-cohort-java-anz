package com.mars.training;

public class MyClass {
	public static void main(String args[]) {
		
		//System.out.println("Main method");
		
		String myString = "hello"; //String literal
		String strObj = new String("World"); // String object
		
		System.out.println(myString.concat(" world"));
		
		
		String myString1 = "Welcome";
		myString1.concat(" back");  // you have to assign it. Strings are immutable
		System.out.println(myString1);
		
		String myString3 = "Welcome";
		myString3 = myString3.concat(" back");  // you have to assign it. Strings are immutable
		System.out.println(myString3);
		
		
		StringBuffer sib = new StringBuffer("abc");
		
		sib.append("Tech"); // Stringbuffer is mutable.
		
		System.out.println(sib);
		
	}
}
	
//	static {
//		System.out.println("Static block");
//	}
//	static {
//		System.out.println("Second block");
//	}
//
//	
//	}
	


